Steps to run the project: 

1. Run npm install or npm i.
2. Run node index.js or nodemon (if you have nodemon installed).
3. Open browser at localhost:8080.

----------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------

Open License used because I do not want to obstruct any one from using any part of the assignment. They are free to use the elements of the assignment as they like.